const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
	entry: [
		'./src/js/index.js',
		'./src/less/index.less'
	],
	output: {
		path: path.resolve(__dirname, 'dev'),
		filename: 'pure-timepicker.js',
		publicPath: '/'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
					options: {	
						presets: [
							'env',
							['es2015', {'modules': false}]
						]
					}
				}
			},
			{
				test: /\.less$/,
				use: ['style-loader', 'css-loader', 'less-loader']
			}
		]
	},
	devtool: 'inline-source-map',
	plugins: [
		new webpack.LoaderOptionsPlugin({debug: true}),
		new HtmlWebpackPlugin({template: './src/index.html'})
	],
	devServer: {
		contentBase: path.join(__dirname, 'dev'),
		compress: true,
		port: 9090
	}
};
